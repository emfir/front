import './App.css';
import Navigation from './components/Navigation/Navigation';
import Logo from './components/Logo/Logo';
import ImageLinkForm from './components/ImageLinkForm/ImageLinkForm';
import Rank from './components/Rank/Rank';
import FaceRecognition from './components/FaceRecognition/FaceRecognition';
import Particles from "react-particles-js";
import {Component} from "react";
import Clarifai from "clarifai"
import SignIn from "./components/Signin/Signin";
import Register from "./components/Register/Register";

const app = new Clarifai.App({
    apiKey: '838c3d079d09488b8885685989359b99'
})

const particleOptions = {
    particles: {
        number: {
            value: 80,
            density: {
                enable: true,
                value_area: 800
            }
        }
    }
}


class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            input: "",
            imageUrl: '',
            box: {},
            route: "signin",
            isSignedIn: false,
            user: {
                id: '',
                name: '',
                email: '',
                entries: 0,
                joined: ''
            }
        }
    }

    componentDidMount() {
        fetch("https://still-shelf-34812.herokuapp.com")
            .then(response => response.json())
            .then(console.log)
    }

    onUserLoad = (data) => {
        this.setState({
            user: {
                id: data.id,
                name: data.name,
                email: data.email,
                entries: data.entries,
                joined: data.joined
            }
        })
    }

    calculateFaceLocation = (data) => {
        const clarifaiFace = data.outputs[0].data.regions[0].region_info.bounding_box
        const image = document.getElementById('inputImage');
        const width = Number(image.width)
        const height = Number(image.height)
        return {
            leftCol: clarifaiFace.left_col * width,
            topRow: clarifaiFace.top_row * height,
            rightCol: width - (clarifaiFace.right_col * width),
            bottomRow: height - (clarifaiFace.bottom_row * height)
        }

    }

    displayFaceBox = (box) => {
        this.setState({box: box});
    }

    onInputChange = (event) => {
        this.setState({input: event.target.value})
    }

    onButtonSubmit = () => {
        this.setState({imageUrl: this.state.input});
        app.models.predict('d02b4508df58432fbb84e800597b8959',
            this.state.input)
            .then(response => {
                    this.displayFaceBox(this.calculateFaceLocation(response))

                    if (response) {
                        fetch('https://still-shelf-34812.herokuapp.com/image', {
                            method: 'put',
                            headers: {'Content-Type': 'application/json'},
                            body: JSON.stringify({
                                    id: this.state.user.id
                                }
                            )
                        }).then(response => response.json())
                            .then(data => {
                            if (Number.isInteger(parseInt(data))) {
                                this.setState(Object.assign(this.state.user, {entries: data}))
                            }
                        })
                    }
                }
            ).catch(err => console.log(err));
    }

    onRouteChange = (route) => {
        if (route === "home") {
            this.setState({isSignedIn: true})
        } else if (route === "signin") {
            this.setState({isSignedIn: false})
        }


        this.setState({route: route});
    }

    render() {
        const {box, imageUrl, isSignedIn, route} = this.state;
        const {name, entries} = this.state.user

        let rend = {
            "signin": <SignIn onUserLoad={this.onUserLoad} onRouteChange={this.onRouteChange}/>,
            "home":
                <div>
                    <Logo/>
                    <Rank name={name} entries={entries}/>
                    <ImageLinkForm
                        onInputChange={this.onInputChange}
                        onSubmit={this.onButtonSubmit}
                    />
                    <FaceRecognition box={box} imageUrl={imageUrl}/>
                </div>,
            "register": <Register onUserLoad={this.onUserLoad} onRouteChange={this.onRouteChange}/>
        }


        return (
            <div className="App">
                <Particles className='particles'
                           params={particleOptions}
                />
                <Navigation isSignedIn={isSignedIn} onRouteChange={this.onRouteChange}/>
                {rend[route]}
            </div>
        );
    }
}

export default App;
